package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size)
    {
        for (int i = 0; i < size - 1; i++)
        {
            int min = i;
            for (int j = i + 1; j < size; j++)
            {
                if (arr[j] < arr[min])
                {
                    min = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size)
    {
        for (int i = 1; i < size; i++)
        {
            int temp = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > temp)
            {
                arr[j+1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size)
    {
        halve(arr, 0, size - 1);
        return arr;
    }

    public void halve(int[] arr,int firstIndex,int lastIndex)
    {
        if (firstIndex < lastIndex)
        {
            int middleIndex = (firstIndex + lastIndex) / 2;
            halve(arr, firstIndex, middleIndex);
            halve(arr, middleIndex + 1, lastIndex);
            sort(arr, firstIndex, middleIndex, lastIndex);
        }
    }

    public void sort(int[] arr, int firstIndex, int middleIndex, int lastIndex)
    {
        int k;
        int temp1 = 0;
        int temp2 = 0;
        int[] firstTempArray = new int[middleIndex - firstIndex + 1];
        int[] secondTempArray = new int[lastIndex - middleIndex];
        for (int i = 0; i < firstTempArray.length; i++)
        {
            firstTempArray[i] = arr[firstIndex + i];
        }
        for (int j = 0; j < secondTempArray.length; j++)
        {
            secondTempArray[j] = arr [middleIndex + j + 1];
        }
        for (k = firstIndex; temp1 < firstTempArray.length && temp2 < secondTempArray.length; k++)
        {
            arr[k] = firstTempArray[temp1] <= secondTempArray[temp2] ? firstTempArray[temp1++] : secondTempArray[temp2++];
        }
        while (temp1 < firstTempArray.length)
        {
            arr[k++] = firstTempArray[temp1++];
        }
        while (temp2 < secondTempArray.length)
        {
            arr[k++] = secondTempArray[temp2++];
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value)
    {
        return iterativeBinarySearch(selectionSort(arr, arr.length), 0, arr.length - 1, value);
    }

    public int iterativeBinarySearch(int[] arr, int firstIndex, int lastIndex, int value)
    {
        int middleIndex = (firstIndex + lastIndex) / 2;
        while (firstIndex <= lastIndex)
        {
            if (arr[middleIndex] < value)
            {
                firstIndex = middleIndex + 1;
            }
            else if (arr[middleIndex] > value)
            {
                lastIndex = middleIndex - 1;
            }
            else
            {
                return middleIndex;
            }
            middleIndex = (firstIndex + lastIndex) / 2;
        }
        return  -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value)
    {
        return recursiveBinarySearch(selectionSort(arr, arr.length), 0, arr.length - 1, value);
    }

    public int recursiveBinarySearch(int[] arr, int firstIndex, int lastIndex, int value)
    {
        if (firstIndex <= lastIndex)
        {
            int middleIndex = firstIndex + ((lastIndex - firstIndex) / 2);
            if (arr[middleIndex] < value)
            {
                return recursiveBinarySearch(arr, middleIndex + 1, lastIndex, value);
            }
            else if (arr[middleIndex] > value)
            {
                return recursiveBinarySearch(arr, firstIndex, middleIndex - 1, value);
            }
            else
            {
                return middleIndex;
            }
        }
        return -1;
    }

}
