package sbu.cs;

public class BlackFunctions
{
    public String function(String str, int arg)
    {
        switch (arg)
        {
            case 1 :
            {
                str = new StringBuilder(str).reverse().toString();
                break;
            }
            case 2 :
            {
                StringBuilder ssttrr = new StringBuilder();
                for (int i = 0; i < str.length(); i++)
                {
                    char ch = str.charAt(i);
                    ssttrr.append(ch).append(ch);
                }
                str = ssttrr.toString();
                break;
            }
            case 3 :
            {
                str = str + str;
                break;
            }
            case 4 :
            {
                StringBuilder rst = new StringBuilder(String.valueOf(str.charAt(str.length() - 1)));
                for (int i = 0; i < str.length() - 1; i++)
                {
                    rst.append(str.charAt(i));
                }
                str = rst.toString();
                break;
            }
            case 5 :
            {
                StringBuilder temp = new StringBuilder(str);
                for (int i = 0; i < str.length(); i++)
                {
                    int num = str.charAt(i) - 97;
                    temp.setCharAt(i, (char) (122 - num));
                }
                str = temp.toString();
                break;
            }
        }
        return str;
    }
}
