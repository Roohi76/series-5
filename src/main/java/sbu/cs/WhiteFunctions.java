package sbu.cs;

public class WhiteFunctions
{
    public String function(String str1, String str2, int arg)
    {
        String str = "";
        switch (arg)
        {
            case 1 :
            {
                int n = Math.min(str1.length(), str2.length());
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < n; i++)
                {
                    strBuilder.append(str1.charAt(i)).append(str2.charAt(i));
                }
                if (str1.length() == n)
                {
                    strBuilder.append(str2.substring(n));
                }
                else
                {
                    strBuilder.append(str1.substring(n));
                }
                str = strBuilder.toString();
                break;
            }
            case 2 :
            {
                str = str + str1 + new StringBuilder(str2).reverse().toString();
                break;
            }
            case 3 :
            {
                int n = Math.min(str1.length(), str2.length());
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < n; i++)
                {
                    strBuilder.append(str1.charAt(i)).append(str2.charAt(str2.length() - i - 1));
                }
                if (str1.length() == n)
                {
                    strBuilder.append(new StringBuilder(str2.substring(n)).reverse().toString());
                }
                else
                {
                    strBuilder.append(str1.substring(n));
                }
                str = strBuilder.toString();
                break;
            }
            case 4 :
            {
                if (str1.length() % 2 == 0)
                {
                    str = str1;
                }
                else
                {
                    str = str2;
                }
                break;
            }
            case 5 :
            {
                int n = Math.min(str1.length(), str2.length());
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < n; i++)
                {
                    char ch = (char) (((str1.charAt(i) + str2.charAt(i) - 194) % 26) + 97);
                    strBuilder.append(ch);
                }
                if (str1.length() == n)
                {
                    strBuilder.append(str2.substring(n));
                }
                else
                {
                    strBuilder.append(str1.substring(n));
                }
                str = strBuilder.toString();
                break;
            }
        }
        return str;
    }
}
