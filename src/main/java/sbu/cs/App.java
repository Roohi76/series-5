package sbu.cs;

public class App {
    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input)
    {
        String[][] str = new String[n][n];
        str[0][0] = input;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if ((i == 0 && j < n-1) || (j == 0 && i < n-1))
                {
                    // Green Cells
                    BlackFunctions func = new BlackFunctions();
                    str[i+1][j] = func.function(str[i][j], arr[i][j]);
                    str[i][j+1] = func.function(str[i][j], arr[i][j]);
                    str[i][j] = func.function(str[i][j], arr[i][j]);
                }
                else if (i == 0 && j == n-1)
                {
                    // Top Right Yellow Cell
                    BlackFunctions func = new BlackFunctions();
                    str[i][j] = func.function(str[i][j], arr[i][j]);
                }
                else if (j == 0 && i == n-1)
                {
                    // Bottom Left Yellow Cell
                    BlackFunctions func = new BlackFunctions();
                    str[i][j+1] = func.function(str[i][j], arr[i][j]);
                }
                else if (j == n-1 && i > 0)
                {
                    // Right Side Pink Cells
                    WhiteFunctions func = new WhiteFunctions();
                    str[i][j] = func.function(str[i][j], str[i-1][j], arr[i][j]);
                }
                else if (i == n-1 && j > 0 && j < n-1)
                {
                    // Bottom Side Pink Cells
                    WhiteFunctions func = new WhiteFunctions();
                    str[i][j+1] = func.function(str[i][j], str[i-1][j], arr[i][j]);
                }
                else
                {
                    // Blue Cells
                    BlackFunctions func = new BlackFunctions();
                    str[i][j+1] = func.function(str[i][j], arr[i][j]); // Left to Right Functions
                    str[i][j] = func.function(str[i-1][j], arr[i][j]); // Top to Bottom Functions
                }
            }
        }
        return str[n-1][n-1];
    }
}
